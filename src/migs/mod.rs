use sqlx::{Sqlite, Pool};

use crate::Versions;

mod v0102;

pub async fn run(db: &Pool<Sqlite>, mut current: Versions, target: Versions, domain: &str) -> Result<(), Box<dyn std::error::Error>> {
    if current == Versions::Unknown || target == Versions::Unknown {
        panic!("Unknown version")
    }

    log::info!("target version: {}", target);
    while current < target {
        log::info!("current version: {}", current);

        match current {
            Versions::V0p1 => v0102::migrate(db, domain).await?,
            _ => panic!("")
        }

        // current += 1
        current = current.next();
    }
    Ok(())
}