use std::fmt::Display;

use chrono::{FixedOffset, TimeZone, Utc, DateTime};
use serde::Serialize;
use sqlx::{Pool, Sqlite};
use uuid::Uuid;

fn make_activity_id(domain: &str, datetime: &DateTime<Utc>) -> String {
    format!("https://{domain}/a/{}", datetime.timestamp_nanos())
}
#[derive(sqlx::FromRow)]
struct DbUserData {
    id: i64,
    preferred_username: String,
}

// old
#[derive(sqlx::FromRow)]
struct DbOldNote {
    pub user_id: i64,
    pub user_note_id: i64,
    pub content: String,
    pub timezone: Option<i64>,
    pub in_reply_to: Option<String>,
    pub published: chrono::NaiveDateTime,
    pub updated_at: Option<chrono::NaiveDateTime>,
}
#[derive(Debug, sqlx::FromRow)]
struct DbOldAttachment {
    pub user_id: i64,
    pub note_id: i64,
    pub blob_id: String,
}
#[derive(sqlx::FromRow)]
struct DbInboxEntry {
    pub id: String,
    pub activity_type: String,
    pub actor: String,
    pub published: chrono::NaiveDateTime,
    pub object: String,
    pub receive_user: i64,
    pub received_at: chrono::NaiveDateTime,
    pub updated_at: Option<chrono::NaiveDateTime>,
    pub update_activity_id: Option<String>,
}

// new
#[derive(Debug, Serialize)]
pub struct NoteObject {
    #[serde(rename = "@context")]
    context: String,
    #[serde(rename = "type")]
    n_type: String,
    id: String,
    #[serde(rename = "attributedTo")]
    attributed_to: String,
    #[serde(rename = "inReplyTo")]
    in_reply_to: Option<String>,
    attachment: Option<Vec<MediaObject>>,
    content: String,
    published: chrono::DateTime<FixedOffset>,
    replies: Option<String>,
    to: Vec<String>
}
impl NoteObject {
    pub fn new(
        domain: &str,
        pref_name: String,
        content: String,
        timezone: Option<i64>,
        published: chrono::NaiveDateTime,
        to: Option<Vec<String>>,
        in_reply_to: Option<String>,
        attachment: Option<Vec<MediaObject>>
    ) -> Self {
        let tz = FixedOffset::east_opt((timezone.unwrap_or(0i64) * 3600) as i32).unwrap();
        let published_utc = Utc.from_utc_datetime(&published);
        let to = to.unwrap_or(vec![String::from("https://www.w3.org/ns/activitystreams#Public")]);

        Self {
            context: String::from("https://www.w3.org/ns/activitystreams"),
            n_type: String::from("Note"),
            id: make_activity_id(domain, &published_utc),
            attributed_to: format!("https://{domain}/u/{pref_name}"),
            in_reply_to,
            content,
            published: published_utc.with_timezone(&tz),
            to,
            replies: None,
            attachment,
        }
    }
}
impl Display for NoteObject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", serde_json::to_string(self).map_err(|_| std::fmt::Error)?)
    }
}

#[derive(Clone, Debug, Serialize)]
pub struct MediaObject {
    #[serde(rename = "type")]
    pub m_type: String,
    #[serde(rename = "mediaType")]
    pub media_type: String,
    /// URL to media
    pub url: String
}

struct DbActivityReceivedUser {
    pub object_id: String,
    pub received_user_id: i64,
    pub received_at: chrono::NaiveDateTime,
}

struct DbNewAttachment {
    pub object_id: String,
    pub blob_id: String,
}
struct DbActivity {
    pub object_id: String,
    /// raw json string. DONT USE THIS DIRECTLY
    pub object_json: String,
    pub created_at: chrono::NaiveDateTime,
    pub updated_at: Option<chrono::NaiveDateTime>,
    pub deleted_at: Option<chrono::NaiveDateTime>,
}

pub(super) async fn migrate(db: &Pool<Sqlite>, domain: &str) -> Result<(), Box<dyn std::error::Error>> {
    // get data
    let notes: Vec<DbOldNote> = sqlx::query_as(
        r#"
select * from notes
        "#)
        .fetch_all(db)
        .await?;
    let attachments: Vec<DbOldAttachment> = sqlx::query_as(
        r#"
select * from attachments
        "#)
        .fetch_all(db)
        .await?;
    let inboxes: Vec<DbInboxEntry> = sqlx::query_as(
        r#"
select * from inboxes  
        "#)
        .fetch_all(db)
        .await?;

    // sub data. not delete
    let users: Vec<DbUserData> = sqlx::query_as(
        r#"
select
    distinct id, preferred_username
from users
inner join notes
on notes.user_id = users.id
        "#)
        .fetch_all(db)
        .await?;

    // create new table
    let mut transaction = db.begin().await?;
    sqlx::query(
        r#"
create table activities (
    object_id   text     not null primary key ,
    object_json text     not null , -- use as json...
    created_at  datetime not null default current_timestamp,
    updated_at  datetime ,
    deleted_at  datetime 
)
        "#)
        .execute(&mut *transaction)
        .await?;
    sqlx::query(
        r#"
create table activities_received_users (
    object_id        text     not null ,
    received_user_id integer  not null ,

    received_at      datetime not null default current_timestamp ,

    foreign key (object_id) references activities(object_id),
    foreign key (received_user_id) references users(id)
)
        "#)
        .execute(&mut *transaction)
        .await?;
    // drop
    sqlx::query(
        r#"
drop table attachments
        "#)
        .execute(&mut *transaction)
        .await?;
    sqlx::query(
        r#"
create table attachments (
    object_id text not null ,
    blob_id text not null ,

    foreign key (object_id) references activities(object_id)
    foreign key (blob_id) references blobs(id)
)
        "#)
        .execute(&mut *transaction)
        .await?;

    // process data to new type
    let notes_new = notes.into_iter().map(|v| {
        let attachments = attachments
            .iter()
            .filter(|a| a.note_id == v.user_note_id && a.user_id == v.user_id)
            .map(|a| {
                let uuid = Uuid::parse_str(&a.blob_id).expect(&format!("bad uuid. {:?}", a));
                MediaObject {
                    m_type: String::from("Document"),
                    media_type: String::from("application/octet-stream"),
                    url: format!("https://{domain}/m/{}", uuid.as_simple()),
                }
            })
            .collect::<Vec<_>>();
        DbActivity {
            object_id: format!("https://{domain}/a/{}", v.published.timestamp_nanos()),
            object_json: NoteObject::new(
                domain, 
                users.iter()
                    .find(|n| n.id == v.user_id)
                    .expect(&format!("prefname not found. uid:{}", v.user_id))
                    .preferred_username.clone(), 
                v.content, 
                v.timezone, 
                v.published, 
                None, 
                v.in_reply_to, 
                if attachments.len() > 0 { Some(attachments) } else { None }
            ).to_string(),
            created_at: v.published,
            updated_at: v.updated_at,
            deleted_at: None,
        }
    }).collect::<Vec<_>>();

    let (inboxes_new, received_users): (Vec<_>, Vec<_>) = 
        inboxes.into_iter().map(|v| {(
            DbActivity {
                object_id: v.id.clone(),
                object_json: v.object,
                created_at: v.received_at,
                updated_at: v.updated_at,
                deleted_at: None,
            },
            DbActivityReceivedUser {
                object_id: v.id,
                received_user_id: v.receive_user,
                received_at: v.received_at,
            }
        )}).unzip();
    
    async fn insert_activity(transaction: &mut sqlx::Transaction<'_, Sqlite>, n: DbActivity) -> Result<(), sqlx::Error> {
        sqlx::query(
            r#"
insert into activities (
    object_id, object_json, created_at, updated_at, deleted_at
) VALUES (
    ?, ?, ?, ?, ?
)
            "#)
            .bind(n.object_id).bind(n.object_json).bind(n.created_at).bind(n.updated_at).bind(n.deleted_at)
            .execute(&mut **transaction)
            .await.map(|_| ())
    }

    // insertion

    // notes
    for e in notes_new {
        insert_activity(&mut transaction, e).await?;
    }
    // inboxes
    for e in inboxes_new {
        insert_activity(&mut transaction, e).await?;
    }
    // received users
    for e in received_users {
        sqlx::query(
            r#"
insert into activities_received_users (
    object_id, received_user_id, received_at
) VALUES (
    ?, ?, ?
)
            "#)
            .bind(e.object_id).bind(e.received_user_id).bind(e.received_at)
            .execute(&mut *transaction)
            .await?;
    }

    // drop table
    sqlx::query(r#"drop table notes"#).execute(&mut *transaction).await?;
    sqlx::query(r#"drop table inboxes"#).execute(&mut *transaction).await?;

    transaction.commit().await?;
    log::info!("Successfully upgraded.");
    Ok(())
}