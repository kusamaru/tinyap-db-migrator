mod migs;
mod ident;
mod versions;
use chrono::Local;
pub use versions::*;

use clap::Parser;

#[derive(Parser)]
struct Args {
    /// path to database file
    path: String,
    /// domain of your server(e.g. example.com, aaa.example.org)
    domain: String,
    /// migration target version
    #[arg(long)]
    target: Option<String>,
    /// dry-run mode(test run)
    #[arg(long)]
    dry_run: bool
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    std::env::set_var("RUST_LOG", "info");
    env_logger::init();

    let args = Args::parse();

    let db = if args.dry_run {
        log::info!("copying database...");
        let p = format!("{}_dry_run.db3", &args.path);
        std::fs::copy(&args.path, &p)?;
        log::info!("copy is done. file is available on {}", &p);

        
        sqlx::sqlite::SqlitePoolOptions::new()
            .connect(&format!(r#"sqlite://{}?mode=rwc"#, &p)).await?
    } else {
        log::info!("backup database...");
        let now = Local::now().timestamp();
        std::fs::copy(&args.path, format!("{}_backup_{}", &args.path, now))?;
        log::info!("backup is done. file is available on {}_backup_{}", &args.path, now);

        sqlx::sqlite::SqlitePoolOptions::new()
            .connect(&format!(r#"sqlite://{}?mode=rwc"#, &args.path)).await?
    };

    let target_version = match args.target.as_deref() {
        None => Versions::default(),
        Some("0.2") => Versions::V0p2,
        Some("0.1") => {
            log::error!("Error: target version must be v0.2~");
            return Ok(())
        },
        _ => {
            log::error!("Error: bad target version specified");
            return Ok(())
        }
    };
    let current_version = ident::ident_current(&db).await?;

    log::info!("running migration...");
    migs::run(&db, current_version, target_version, &args.domain).await?;
    log::info!("migration is done.");

    Ok(())
}
