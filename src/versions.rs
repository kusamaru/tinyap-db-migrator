use std::fmt::Display;

#[derive(Debug, Default, PartialEq, PartialOrd)]
#[repr(usize)]
pub enum Versions {
    Unknown = 999,
    V0p1 = 1,
    #[default] // set to latest
    V0p2 = 2,
}
impl Display for Versions {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", 
            match self {
                Self::Unknown => "Unknown",
                Self::V0p1 => "v0.1",
                Self::V0p2 => "v0.2",
            }
        )
    }
}
impl Versions {
    pub fn next(self) -> Self {
        match self {
            Self::Unknown => self,
            Self::V0p1 => Self::V0p2, 
            Self::V0p2 => self
        }
    }
}