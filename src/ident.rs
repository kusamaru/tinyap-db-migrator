//! identify current db version

use sqlx::{Pool, Sqlite, Row};

use crate::Versions;

pub async fn ident_current(db: &Pool<Sqlite>) -> Result<Versions, Box<dyn std::error::Error>> {

    // is DB v0.1?
    let r1_1 = sqlx::query(
        r#"
select 
    count(*) as count 
from sqlite_master
where type = 'table' and
      name = 'notes'
        "#)
        .fetch_one(db)
        .await;

    let r1_2 = sqlx::query(
        r#"
select 
    count(user_id) as count
from attachments
        "#)
        .fetch_one(db)
        .await;

    match (
        r1_1.and_then(|r| r.try_get::<i64, &str>("count")),
        r1_2.and_then(|r| r.try_get::<i64, &str>("count")),
    ) {
        (Ok(1), Ok(_)) => {
            log::info!("DB is v0.1.");
            return Ok(Versions::V0p1)
        },
        (Ok(0), Err(sqlx::Error::ColumnNotFound(_))) => {/* no such table 'notes' = not v0.1 */},
        (Err(e1), Err(e2)) => {
            log::error!("DB Error: {}", e1.to_string());
            log::error!("DB Error: {}", e2.to_string());
            return Err(e1.into())
        },
        (v1, v2) => {
            log::error!("Error: bad value.");
            log::error!("v1: {:?}\nv2: {:?}", v1, v2);
            panic!()
        }
    }

    // is db v0.2?
    let r2_1 = sqlx::query(
        r#"
select
    count(*) as count
from sqlite_master
where type = 'table' and
      name = 'activities'
        "#)
        .fetch_one(db)
        .await;
    
    match r2_1.and_then(|r| r.try_get::<i64, &str>("count")) {
        Ok(1..) => {
            // TODO: 
            log::info!("DB is v0.2~.");
            return Ok(Versions::V0p2);
        },
        Err(e) => {
            log::error!("DB Error: {}", e.to_string());
            return Err(e.into())
        },
        _ => {
            panic!("undefined error")
        }
    }

    Ok(Versions::Unknown)
}